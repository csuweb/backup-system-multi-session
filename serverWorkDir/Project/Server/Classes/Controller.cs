﻿using System;
using System.Globalization;
using System.IO;

namespace FilemapBackupSystem.Classes
{
    class Controller
    {
        private readonly DataModelEditables dataModelEditables;

        public Controller()
        {
            dataModelEditables = new DataModelEditables();
        }

        // 1. на сервер отправляем карту копирования. 
        //      *если она пустая - делим весь файл на чанки, формируем из этого 
        // copyFilemap(без хэшей), передаем
        // карту и все чанки клиенту
        //      *если не пустая - считываем ее, делим локальный файл на части
        // среди карты ксор ищем схожие области. если схожая область из xorFilemap:
        //          * статичная - читаем эту область из файла, считаем ее хэш, сравниваем с
        //          хэшем из xorFilemap. если не совпали - добавляем в copyFilemap
        //          * иные виды совпавших областей добавляем в copyFilemap
        // если схожей области не нашлось - добавляем ее в copyFilemap как Extra
        // 2. сохраняем xorFilemap
        // 3. извлечь области, вошедшие в copyFilemap, в ChunksTempDir
        public void ServerWork()
        {
            dataModelEditables.Server.SetPath(Path.Combine(dataModelEditables.RootDir, dataModelEditables.FilesDirName));
            dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.ChunkSize, false);

            // принять xorFilemap от клиента
            dataModelEditables.Server.WaitClient();
            dataModelEditables.Server.ReceiveFile();
            LoadFilemap(FilemapTypes.XorFilemap);
            Console.WriteLine("Ксор-карта получена и считана успешно");

            // составить карту копирования
            dataModelEditables.FileWorker.GenerateCopyFilemap(dataModelEditables.LocalFile, dataModelEditables.XorFilemap, ref dataModelEditables.CopyFilemap);
            SaveFilemap(FilemapTypes.CopyFilemap);
            Console.WriteLine(string.Format("Копи-карта создана успешно, областей всего - {0}", dataModelEditables.CopyFilemap.Chunks.Count));

            // отослать карту копирования клиенту
            dataModelEditables.Server.WaitClient();
            dataModelEditables.Server.SendFile(new FileInfo(dataModelEditables.CopyFilemap.FilemapName));
            Console.WriteLine("Копи-карта отправлена");

            // извлечь измененные чанки
            dataModelEditables.FileWorker.ExtractChunks(dataModelEditables.LocalFile, dataModelEditables.CopyFilemap,
                                                        dataModelEditables.ChunksTempDirectoryName,
                                                        dataModelEditables.ReadWriteBuffer);
            Console.WriteLine("Чанки для копирования извлечены");

            // отправить измененные чанки клиенту
            for (int i = 0; i < dataModelEditables.CopyFilemap.Chunks.Count; i++)
            {
                dataModelEditables.Server.WaitClient();
                dataModelEditables.Server.SendFile(new FileInfo(Path.Combine(dataModelEditables.ChunksTempDirectoryName, dataModelEditables.CopyFilemap.Chunks[i].Id.ToString(CultureInfo.InvariantCulture))));
            }
        }

        public void SaveFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.CopyFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.XorFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.XorFilemap);
                    break;
            }
        }

        public void LoadFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.CopyFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.XorFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.XorFilemap);
                    break;
            }
        }
    }
}
