﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FilemapBackupSystem.Classes;

namespace FilemapBackupSystem.Classes
{
    class Utils
    {
        public static void MemSet(byte[] array, byte value)
        {
            if (array == null)
                throw new ArgumentNullException("array");

            int block = 32, index = 0;
            int length = Math.Min(block, array.Length);

            while (index < length)
                array[index++] = value;

            length = array.Length;
            while (index < length)
            {
                Buffer.BlockCopy(array, 0, array, index, Math.Min(block, length - index));
                index += block;
                block *= 2;
            }
        }
    }
}

namespace ExtensionMethods
{
    using System.Collections.Generic;

    public static class MyExtensions
    {
        public static void Replace(this List<Chunk> list, int index, Chunk entry)
        {
            list.RemoveAt(index);
            list.Insert(index, entry);
        }
    }
}
