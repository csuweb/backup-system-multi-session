﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    public class Server : Client
    {
        public TcpListener sListener { get; set; }
        public TcpClient sClient { get; set; }
        public NetworkStream sStream { get; set; }
        public string sFilesDir;

        #region Receive

        public Server()
        {
            sListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 1488);
        }

        public void SetPath(string filesDir)
        {
            this.sFilesDir = filesDir;
        }

        public void WaitClient()
        {
            sListener.Start();
            Console.WriteLine("Сервер ожидает подключение на " +
                              IPAddress.Parse(((IPEndPoint)sListener.LocalEndpoint).Address.ToString()) + " : " +
                              ((IPEndPoint)sListener.LocalEndpoint).Port.ToString(CultureInfo.InvariantCulture));
            sClient = sListener.AcceptTcpClient();
            sStream = sClient.GetStream();
        }


        public void ReceiveFile()
        {
            byte[] buffer = new byte[sClient.ReceiveBufferSize];
            try
            {
                int bytesRead = sStream.Read(buffer, 0, 12);
                if (bytesRead == 0) return;

                ushort id = BitConverter.ToUInt16(buffer, 0);
                long len = BitConverter.ToInt64(buffer, 2);
                ushort nameLen = BitConverter.ToUInt16(buffer, 10);

                sStream.Read(buffer, 0, nameLen);

                string fileName = Encoding.UTF8.GetString(buffer, 0, nameLen);

                if (id == 1)
                {
                    using (BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(sFilesDir, fileName), FileMode.Create)))
                    {
                        int recieved = 0;
                        while (recieved < len)
                        {
                            bytesRead = sStream.Read(buffer, 0, sClient.ReceiveBufferSize);
                            recieved += bytesRead;
                            writer.Write(buffer, 0, bytesRead);
                            //Console.WriteLine("{0} bytes recieved.", recieved);
                        }
                    }
                    Console.WriteLine(string.Format("Входящий файл принят - \"{0}\", [{1} байт]", fileName, len));
                }
                else
                {
                    Console.WriteLine(fileName);
                }
            }
            catch (Exception)
            {
                sStream.Close();
                sClient.Close();
            }
            finally
            {
                sStream.Flush();
            }
        }

        #endregion Receive

        #region Send

        public void SendFile(FileInfo file)
        {
            cStream = sClient.GetStream();
            cClient = sClient;
            cFilesDir = sFilesDir;
            base.SendFile(file);
            sClient.Close();
        }

        public void SendMessage(string message)
        {
            cClient = sClient;
            base.SendMessage(message);
            sClient.Close();
        }

        #endregion Send
    }
}
