﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FilemapBackupSystem.Classes;

namespace FilemapBackupSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Exceptioner exr = new Exceptioner();
            try
            {
                Controller controller = new Controller();
                controller.ClientWork();
                //controller.ServerWork();
                //controller.SplitLocalFile();
                //controller.SplitReceivedFile();
                //controller.XorLocalAndReceivedFiles();
                //controller.GenerateXorFilemapHashes();
                //controller.SaveFilemap(FilemapTypes.xorFilemap);
                //controller.GenerateCopyFilemap();
                //controller.SaveFilemap(FilemapTypes.copyFilemap);

                //controller.ExtractChunks();
                //controller.MergeChunks();
            }
            catch (Exception ex)
            {
                exr.CatchExceprion(ex);
            }
            finally
            {
                Console.WriteLine("Нажмите любую кнопку...");
                Console.ReadKey();
            }
        }
    }
}
