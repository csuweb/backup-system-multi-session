﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    class Controller
    {
        private readonly DataModelEditables dataModelEditables;

        public Controller()
        {
            this.dataModelEditables = new DataModelEditables();
        }

        // с сервера переданы чанки файла в ChunksTempDir, локальный файл
        // отсутствует
        // 0. считать полученный от сервера copyFilemap
        // 1. слить эти чанки в правильной последовательности
        // 2. поделить полученный файл на чанки, посчитать хэши чанков
        // 3. сохранить карту как xorFilemap
        public void ClientWork()
        {
            
            dataModelEditables.Client.SetPath(Path.Combine(dataModelEditables.RootDir, dataModelEditables.FilesDirName));
            LoadFilemap(FilemapTypes.XorFilemap);

            if (!dataModelEditables.FileWorker.IsFileExists(dataModelEditables.LocalFile) &&
                !dataModelEditables.XorFilemap.IsSplitted)
            {
                // если локальный файл и xorFilemap не были обнаружены - 
                // создать и сохранить пустой xorFilemap
                Console.WriteLine("Ксор-карта не была обнаружена - будет создана пустая");
                SaveFilemap(FilemapTypes.XorFilemap);
            }
            else
            {
                Console.WriteLine("Обнаружена локальная ксор-карта");
                dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.ChunkSize, false);
            }

            // отправить xorFilemap на сервер
            dataModelEditables.Client.Init();
            dataModelEditables.Client.SendFile(new FileInfo(dataModelEditables.XorFilemap.FilemapName));

            // получить copyFilemap от сервера
            dataModelEditables.Client.Init();
            dataModelEditables.Client.ReceiveFile();
            LoadFilemap(FilemapTypes.CopyFilemap);
            Console.WriteLine(string.Format("{0} чанков будут приняты", dataModelEditables.CopyFilemap.Chunks.Count));

            // получить чанки во временную папку
            dataModelEditables.Client.SetPath(dataModelEditables.ChunksTempDirectoryName);
            for (int i = 0; i < dataModelEditables.CopyFilemap.Chunks.Count; i++)
            {
                dataModelEditables.Client.Init();
                dataModelEditables.Client.ReceiveFile();
            }

            // поксорить локальный файл с загруженными чанками
            if (dataModelEditables.FileWorker.IsFileExists(dataModelEditables.LocalFile) &&
                dataModelEditables.XorFilemap.IsSplitted && dataModelEditables.CopyFilemap.IsSplitted)
            {
                Console.WriteLine("Формируется новая ксор-карта на основе полученных данных");
                dataModelEditables.FileWorker.XorFileToChunks(dataModelEditables.LocalFile,
                                                              dataModelEditables.ChunksTempDirectoryName,
                                                              ref dataModelEditables.XorFilemap,
                                                              dataModelEditables.CopyFilemap,
                                                              DataModelConstants.XorUnitSize,
                                                              dataModelEditables.ReadWriteBuffer);
            }

            if (dataModelEditables.CopyFilemap.IsSplitted)
            {
                Console.WriteLine("Полученные чанки сливаются в единный файл");
                dataModelEditables.FileWorker.MergeChunks(dataModelEditables.LocalFile, dataModelEditables.CopyFilemap,
                                                          ref dataModelEditables.MergedFile,
                                                          dataModelEditables.ChunksTempDirectoryName,
                                                          dataModelEditables.ReadWriteBuffer);
            }

            if (!dataModelEditables.XorFilemap.IsSplitted)
            {
                dataModelEditables.FileWorker.SplitFileToChunks(ref dataModelEditables.LocalFile, DataModelConstants.ChunkSize, false);
                Console.WriteLine("Формируется ксор-карта по-умолчанию");
                dataModelEditables.FileWorker.CreateDefaultXorFilemap(dataModelEditables.LocalFile,
                    ref dataModelEditables.XorFilemap, DataModelConstants.ChunkSize);
            }
            SaveFilemap(FilemapTypes.XorFilemap);
            Console.WriteLine("Ксор-карта сохранена");
        }

        // 1. на сервер отправляем карту копирования. 
        //      *если она пустая - делим весь файл на чанки, формируем из этого 
        // copyFilemap(без хэшей), передаем
        // карту и все чанки клиенту
        //      *если не пустая - считываем ее, делим локальный файл на части
        // среди карты ксор ищем схожие области. если схожая область из xorFilemap:
        //          * статичная - читаем эту область из файла, считаем ее хэш, сравниваем с
        //          хэшем из xorFilemap. если не совпали - добавляем в copyFilemap
        //          * иные виды совпавших областей добавляем в copyFilemap
        // если схожей области не нашлось - добавляем ее в copyFilemap как Extra
        // 2. сохраняем xorFilemap
        // 3. извлечь области, вошедшие в copyFilemap, в ChunksTempDir
        public void SaveFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.CopyFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.XorFilemap:
                    dataModelEditables.FilemapWorker.SaveFilemap(dataModelEditables.XorFilemap);
                    break;
            }
        }

        public void LoadFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FilemapTypes.CopyFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.CopyFilemap);
                    break;
                case FilemapTypes.XorFilemap:
                    dataModelEditables.FilemapWorker.LoadFilemap(ref dataModelEditables.XorFilemap);
                    break;
            }
        }
    }
}
