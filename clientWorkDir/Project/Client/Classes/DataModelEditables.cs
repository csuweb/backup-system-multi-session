﻿using System.Collections.Generic;
using System.IO;

namespace FilemapBackupSystem.Classes
{
    class DataModelEditables
    {
        public string Dir = @"02082015";

        public string RootDir = @"C:\Users\Parabellum\Dropbox\Work\Система резервного копирования облака\clientWorkDir";
        public string FilesDirName = @"Files";

        public int ReadWriteBuffer = 10485760; // 10mb


        public string LocalFileName
        {
            get { return Path.Combine(RootDir, FilesDirName, "localFile.exe"); }
        }

        public string MergedFileName
        {
            get { return Path.Combine(RootDir, FilesDirName, "merged.exe"); }
        }

        public string ReceivedFileName
        {
            get { return Path.Combine(RootDir, FilesDirName, "receivedFile.exe"); }
        }

        public string ReceivedFileMapName
        {
            get { return Path.Combine(RootDir, FilesDirName, "receivedFilemap.xml"); }
        }

        public string LocalFileMapName
        {
            get { return Path.Combine(RootDir, FilesDirName, "localFilemap.xml"); }
        }

        public string XorFilemapName
        {
            get { return Path.Combine(RootDir, FilesDirName, "xorFilemap.xml"); }
        }

        public string CopyFilemapName
        {
            get { return Path.Combine(RootDir, FilesDirName, "copyFilemap.xml"); }
        }

        public string ChunksTempDirectoryName
        {
            get { return Path.Combine(RootDir, FilesDirName, "chunksTempDir"); }
        }

        public FileAttributes LocalFile;
        public FileAttributes MergedFile;
        public FileAttributes ReceivedFile;
        public FileAttributes CopyFilemap;
        public FileAttributes XorFilemap;

        public Exceptioner Exceptioner;
        public FileWorker FileWorker;
        public Utils Utils;
        public FilemapWorker FilemapWorker;

        public Client Client;
        public Server Server;

        public DataModelEditables()
        {
            Exceptioner = new Exceptioner();
            FileWorker = new FileWorker();
            Utils = new Utils();
            FilemapWorker = new FilemapWorker();
            Client = new Client();
            Server = new Server();

            LocalFile = new FileAttributes {Chunks = new List<Chunk>(), Filename = LocalFileName};
            MergedFile = new FileAttributes { Chunks = new List<Chunk>(), Filename = MergedFileName};
            ReceivedFile = new FileAttributes { Chunks = new List<Chunk>(), Filename = ReceivedFileName};

            CopyFilemap = new FileAttributes { Chunks = new List<Chunk>(), FilemapName = CopyFilemapName};
            XorFilemap = new FileAttributes { Chunks = new List<Chunk>(), FilemapName = XorFilemapName};
        }


    }
}
