﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FilemapBackupSystem.Classes
{
    public class Client : Server
    {
        public TcpClient cClient;
        public NetworkStream cStream;
        public string cFilesDir;

        public void Init()
        {
            cClient = new TcpClient("localhost", 1488);
            cStream = cClient.GetStream();
            Console.WriteLine("Соединен с сервером");
        }

        public void SetPath(string filesDir)
        {
            cFilesDir = filesDir;
        }

        public void SendFile(FileInfo file)
        {
            byte[] id = BitConverter.GetBytes((ushort)1);
            byte[] size = BitConverter.GetBytes(file.Length);
            byte[] fileName = Encoding.UTF8.GetBytes(file.Name);
            byte[] fileNameLength = BitConverter.GetBytes((ushort)fileName.Length);
            byte[] fileInfo = new byte[12 + fileName.Length];

            id.CopyTo(fileInfo, 0);
            size.CopyTo(fileInfo, 2);
            fileNameLength.CopyTo(fileInfo, 10);
            fileName.CopyTo(fileInfo, 12);

            cStream.Write(fileInfo, 0, fileInfo.Length); //Размер файла, имя

            byte[] buffer = new byte[1024 * 32];
            int count;

            long sended = 0;

            using (FileStream fileStream = new FileStream(file.FullName, FileMode.Open))
                while ((count = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    cStream.Write(buffer, 0, count);
                    sended += count;
                    //Console.WriteLine("{0} bytes sended.", sended);
                }
            Console.WriteLine(string.Format("Файл \"{0}\" отправлен [{1} байт]", file.Name, file.Length));
            cStream.Flush();
            cClient.Close();
        }


        public void SendMessage(string message)
        {
            byte[] id = BitConverter.GetBytes((ushort)0);
            byte[] msg = Encoding.UTF8.GetBytes(message);
            byte[] msgLength = BitConverter.GetBytes((ushort)msg.Length);
            byte[] fileInfo = new byte[12 + msg.Length];

            id.CopyTo(fileInfo, 0);
            msgLength.CopyTo(fileInfo, 10);
            msg.CopyTo(fileInfo, 12);

            cStream.Write(fileInfo, 0, fileInfo.Length);
            cStream.Flush();
            cClient.Close();
        }

        public void ReceiveFile()
        {
            sStream = cClient.GetStream();
            sClient = cClient;
            sFilesDir = cFilesDir;
            base.ReceiveFile();
            cClient.Close();
        }
    }
}
