﻿using System;
using System.Collections.Generic;

namespace FilemapBackupSystem.Classes
{
    public struct Chunk
    {
        public Int32 Id;
        public string Hash;
        public string Type;

        public Int64 Start;
        public Int64 End;
        public Int64 Length;
        public void CopyFrom(Chunk in1)
        {
            Id = in1.Id;
            Hash = in1.Hash;
            Type = in1.Type;

            Start = in1.Start;
            End = in1.End;
            Length = in1.Length;
        }
    }

    public struct XorUnitAttributes
    {
        public bool IsEqual;
        public Int64 DifferenceOffset;
    }

    public struct FileAttributes
    {
        public bool IsSplitted;
        public string FilemapName;
        public string Filename;
        public Int64 FileSize;
        public List<Chunk> Chunks;
        public void CopyFrom(FileAttributes in1)
        {
            if(in1.FilemapName != null) FilemapName = in1.FilemapName;
            Filename = in1.Filename;
            FileSize = in1.FileSize;
            Chunks = new List<Chunk>(in1.Chunks.Count);
            foreach (var chunk in in1.Chunks)
                Chunks.Add(chunk);
        }

        public Chunk GetChunkById(int id)
        {
            Chunk result = new Chunk();
            for (int i = 0; i < Chunks.Count; i++)
            {
                if (Chunks[i].Id == id)
                    result.CopyFrom(Chunks[i]);
            }
            return result;
        }

        public void Destroy()
        {
            IsSplitted = false;
            Chunks.Clear();
            Filename = null;
            FileSize = 0;
        }
    }

    public static class ChunkType
    {
        public const string Static = "Static";
        public const string Variable = "Variable";
        public const string Extra = "Extra";
        public const string DifLen = "DifLen";
        public const string Undef = "Undef";
    }

    public static class FilemapTypes
    {
        public const string LocalFilemap = "localFilemap";
        public const string ReceivedFilemap = "receivedFilemap";
        public const string CopyFilemap = "copyFilemap";
        public const string XorFilemap = "xorFilemap";
    }

    public static class DataModelConstants
    {
        public const Int64 ChunkSize = 5242880; //5 mb
        public const Int64 XorUnitSize = 2097152; //2mb, меньше chunkSize

        public const string ChunksDirectoryName = "chunks";
    }
}
